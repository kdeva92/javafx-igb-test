/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.lorainelab.igb.menu.api.example;

import aQute.bnd.annotation.component.Component;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javax.swing.JFrame;

@Component(immediate = true)
public class MenuBarFxExample  {

    static Button btn;
    static Label helloLabel;

    public  static void initUI(){
        JFXPanel fXPanel = initSwing();
        initFx(fXPanel);
    }
    
    public static JFXPanel initSwing(){
        JFrame swingFrame = new JFrame();
        JFXPanel fxPanel = new JFXPanel();
        swingFrame.add(fxPanel);
        swingFrame.setSize(300, 200);
        swingFrame.setVisible(true);
        //swingFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        return fxPanel;
    }
    
    
    public static void initFx(JFXPanel fXPanel) {
        btn = new Button();
        helloLabel = new Label("Hello world!!");
        helloLabel.setVisible(false);
        btn.setText("Say Hello World'");
        btn.setOnAction(x -> {
            System.out.println("Button clicked " + x);
            showHello();
            //System.exit(1);
        });

        StackPane root = new StackPane();
        root.getChildren().add(btn);
        StackPane.setAlignment(btn, Pos.CENTER);
        root.getChildren().add(helloLabel);

        Scene scene = new Scene(root, 300, 250);

       fXPanel.setScene(scene);
    }

    private static void showHello() {
        helloLabel.setVisible(true);
    }

    /**
     * @param args the command line arguments
     */
    public static void showFx() {
       
        
        System.out.println("call to inint UI");
        Platform.runLater(() -> initUI());
       //launch(MenuBarFxExample.class);
    }

}
